# Inheritance and poly-morphism

This is an evolving handout. For now I will only list the names of the files
provided in this repository, as well as a short description of the point they
are trying to illustrate.

--- 

List of included files:

- [`readme.md`][read-me];
    this _readme_ file (`MarkDown` format).
- [`readme.md.pdf`][read-me-pdf];
    this _readme_ file (`pdf` format).
- [`00-base-class.cpp`][00-example];
    the `UCLA_Person` **base** class with a short description of how `protected`
    members of a class work.
- [`01-der...rof.cpp`][01-example];
    the `UCLA_Student`, and `UCLA_Professor` **derived** classes. Also there is
    a short explanation of `public` _vs_ `protected` _vs_ `private` inheritance.
- [`02-der...-p1.cpp`][02-example];
    illustration of derived object instantiation without explicit definition of
    class constructor.
- [`03-con...ses.cpp`][03-example];
    illustration of derived object instantiation with, and without, explicit
    calls to the base constructor.
- [`04-mul...rit.cpp`][04-example];
    a derived object is created by inheriting from two different base classes.
    Also, a brief description of the `static` keyword.
- [`05-der...-p2.cpp`][05-example];
    manipulation of derived objects via pointers to base objects.
- [`06-der...ken.cpp`][06-example];
    manipulation of derived objects via pointers to base objects. This is broken
    because the base class does not implement the function `id_card()`.
- [`07-der...ken.cpp`][07-example];
    fix to previous example. The key is to implement the missing function in the
    base class. It also introduces the `virtual` keyword.
- [`08-der...ain.cpp`][08-example];
    shows the effect of removing `virtual` from `id_card()` in the base class.
- [`09-der...ain.cpp`][09-example];
    adds `virtual` back to `id_card()` in the base class while taking away its
    definition. The base class is now an _abstract class_.
- [`10-der...ain.cpp`][10-example];
    shows the effect of trying to instantiate an abstract class.
- [`11-abs...oly.cpp`][11-example];
    this is how these two classes are supposed to work: only _professors_ and
    _students_ are _"real"_ entities, wheres a _person_ is only an _"abstract"_
    concept.

[read-me]: readme.md
[read-me-pdf]: readme.md
[00-example]: 00-base-class.cpp
[01-example]: 01-derived-stud-prof.cpp
[02-example]: 02-derived-stud-prof-test-p1.cpp
[03-example]: 03-constr-derived-classes.cpp
[04-example]: 04-multip-inherit.cpp
[05-example]: 05-derived-stud-prof-test-p2.cpp
[06-example]: 06-derived-stud-prof-test-p3-broken.cpp
[07-example]: 07-derived-stud-prof-test-p4-unbroken.cpp
[08-example]: 08-derived-stud-prof-test-p5-broken-again.cpp
[09-example]: 09-derived-stud-prof-test-p6-unbroken-again.cpp
[10-example]: 10-derived-stud-prof-test-p7-un-unbroken-yet-again.cpp
[11-example]: 11-abstract-poly.cpp
